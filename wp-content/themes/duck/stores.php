<?php
    /*
        Template Name: Stores
    */
        $store_page_id = get_queried_object_id();
         
?>
<?php get_header();?>
<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <?php echo '
                    <h1 class="page-title">' 
                        . get_the_title() . 
                    '</h1><div class="shop-header"></div>';
            ?>
            </div>
            <div class="col-md-11 col-md-push-1">
            <?php 
                $stores = get_stores();
                echo '<h2>' . get_field('duck_head_stores_title', $store_page_id) . '</h2>';
                show_stores($stores[1], 3, false);
                $_SESSION['currState'] = '';
            ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11 col-md-push-1">
            <?php
                echo '<h2>' . get_field('retail_partners_title', $store_page_id) . '</h2>';
                show_stores($stores[0], 3);
                $_SESSION['currState'] = '';
            ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11 col-md-push-1">
                <?php
                    echo '<h2>' . get_field('retail_international_partners_title', $store_page_id) . '</h2>';
                    show_stores($stores[2], 3);
                    $_SESSION['currState'] = '';
                ?>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>