<?php 
    /*
        Template Name: Home
    */
?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<main id="main">
    <div class="container">
        <div class="row"><!-- Row 1 -->
            <div class="col-md-12"></div>
            <div class="col-md-8">
                <?php 
                    $id = get_queried_object_id();
                    $img = get_field('row_1_image_left', $id);
                    $link = get_field('row_1_image_left_link', $id); 
                ?>

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                        <?php show_slideshow(); ?>
                  </div>

                  <!-- Controls -->
                  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </div>
            <div class="col-md-4 margin-top-20">
                <?php 
                    $img = get_field('row_1_image_right', $id);
                    $link = get_field('row_1_image_right_link', $id);
                    echo show_image($img, $link, $mini_size='medium');
                ?>
            </div>
        </div><!-- Row 2 -->
        <div class="row staff-picks">
            <div class="col-md-12">
                <div class="staff">
                    <h1 class="page-title">
                        <?php echo get_field('3_star_bar_title', 'option'); ?>
                    </h1>
                    <div class="shop-header"></div>
                </div>
            </div>
        </div>
        <?php show_featured($id); ?>
        <div class="row staff-picks">
            <div class="col-md-12">
                <div class="staff-bottom">
                    <div class="shop-header"></div>
                </div>
            </div>
        </div>

        <!-- Row 3 -->
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-4">
                        <div id="instafeed"></div>
                    </div>
                    <div class="col-md-8 margin-top-20">
                        <?php 
                            $img = get_field('row_2_image_right_top', $id);
                            $link = get_field('row_2_image_right_top_link', $id);
                            echo show_image($img, $link, $mini_size='medium');
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <?php 
                            $img = get_field('row_2_image_left_bottom', $id);
                            $link = get_field('row_2_image_left_bottom_link', $id);
                             echo show_image($img, $link, $mini_size='medium');
                        ?>
                    </div>
                    <div class="col-md-4 margin-top-20">
                        <?php 
                            $img = get_field('row_2_image_right_bottom', $id);
                            $link = get_field('row_2_image_right_bottom_link', $id);
                            echo show_image($img, $link, $mini_size='medium');
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3 margin-top-20">
                <?php 
                    $img = get_field('row_2_image_right_full', $id);
                    $link = get_field('row_2_image_right_full_link', $id);
                     echo show_image($img, $link, $mini_size='medium');
                ?>
            </div>
        </div><!-- Row 3 -->
        <div class="row">
            <div class="col-md-8">
                <?php 
                    $img = get_field('row_4_image_left', $id);
                    $link = get_field('row_4_image_left_link', $id);
                     echo show_image($img, $link, $mini_size='medium');
                ?>
            </div>
            <div class="col-md-4 margin-top-20">
                <?php 
                    $img = get_field('row_4_image_right', $id);
                    $link = get_field('row_4_image_right_link', $id);
                     echo show_image($img, $link, $mini_size='medium');
                ?>
            </div>
        </div>
    </div>
</main>
<?php endwhile; else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
<?php get_footer(); ?>