    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer_menu_1', 'container_class' => 'my_extra_menu_class' ) ); ?>
                </div>
                <div class="col-md-2">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer_menu_2', 'container_class' => 'my_extra_menu_class' ) ); ?>
                </div>
                <div class="col-md-2">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer_menu_3', 'container_class' => 'my_extra_menu_class' ) ); ?>
                </div>
                <div class="col-md-5 col-md-push-1">
                    <?php 
                        $img = get_field('footer_image' , 'options');
                        echo show_image($img);
                    ?>

                    <form  role="form" method="post" id="signup">
                        <p>Leave Us Your Email, Let's Stay In Touch</p>
                        <div id="response"></div>
                        <input type="email"  id="email" name="email" action="<?php bloginfo('template_directory'); ?>/inc/subscribe.php" placeholder="you@yourself.com" value="">
                        <button type="submit">SUBSCRIBE</button>
                    </form>
                </div>
            </div>
        </div>
    </footer>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php show_social_footer(); ?>
                    <p>DUCK HEAD © 2014 Duck Head Apparel 816 S. ELM ST, GREENSBORO, NC 27406 | <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Terms & Conditions' ) ) ); ?>">Terms & Conditions</a></p>
                </div>
            </div>
        </div>
    </div>
    <div id="search">
        <button type="button" class="close">×</button>
        <form action="<?php bloginfo('url'); ?>" id="searchform" method="get">
            <input type="search" id="s" name="s" value="" placeholder="type keyword(s) here" />
            <input type="submit" value="Search" id="searchsubmit" class="btn btn-primary" />
        </form>
    </div>
    <?php wp_footer(); ?>
    </body>
</html>
<?php if(is_front_page()):?>
    <script type="text/javascript">
        var feed = new Instafeed({
            get: "user",
            userId: 1232408184,
            accessToken: "1232408184.467ede5.cb3027c07f4b474ebccb7e607ee23405",
            clientId: "179199f527a64e3b9d820d9d76870336",
            useHttp: true,
            resolution : "standard_resolution",
            limit: 1,
            sortBy: "most-recent",
            template: '<a href="{{link}}"><img src="{{image}}" /></a>'
        });
        feed.run();
    </script>
<?php endif; ?>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        // jQuery Validation
        $("#signup").validate({
            // if valid, post data via AJAX
            submitHandler: function(form) {
                $.post("<?php bloginfo('template_directory'); ?>/inc/subscribe.php", { email: $("#email").val() }, function(data) {
                    $('#response').html(data);
                });
            },
            // all fields are required
            rules: {
                email: {
                    required: true,
                    email: true
                }
            }
        });
    });
    jQuery(function ($) {
        $('a[href="#search"]').on('click', function(event) {
            event.preventDefault();
            $('#search').addClass('open');
            $('#search > form > input[type="search"]').focus();
        });
        
        $('#search, #search button.close').on('click keyup', function(event) {
            if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                $(this).removeClass('open');
            }
        });
    });
</script>