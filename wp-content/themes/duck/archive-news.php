<?php get_header(); ?>

<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-title"><?php post_type_archive_title(); ?></h1>
                <div class="shop-header"></div>
            </div>
        </div>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="row">
                <article>
                    <div class="col-md-4">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="col-md-8">
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <p><?php show_article_url(); ?></p>
                    </div>
                </article>
            </div>
        <?php endwhile; else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php endif; ?>
    </div>
</main>

<?php get_footer(); ?>