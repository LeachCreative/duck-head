<?php 
	echo '<div class="product-colors-listing">';
		if( have_rows('product_color_repeater') ):
		 
		 	// loop through the rows of data
		    while ( have_rows('product_color_repeater') ) : the_row();
		 
		        // display a sub field value
		        ?>
		        <?php 
		        	$color = get_sub_field('product_color');
		        	$image = get_sub_field('product_color_image');
		        ?>
		        	<a href="<?php the_permalink(); ?>">
		        	<?php if($color != '') { ?>
		        		<div style="background-color:<?php the_sub_field('product_color'); ?>"></div></a>
		        	<?php } elseif($image != '') { ?>
		        		<div style="background:url('<?php echo $image['url']; ?>')"></div></a>
		        	 <?php } ?>

		        <?php
		 
		    endwhile;
		 
		else :
		 
		    // no rows found
		 
		endif;
	echo '</div>';
?>