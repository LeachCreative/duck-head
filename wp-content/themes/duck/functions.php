<?php

	register_nav_menus( array(
		'main_menu' => 'Main Menu',
		'footer_menu_1' => 'Footer 1',
		'footer_menu_2' => 'Footer 2',
		'footer_menu_3' => 'Footer 3'
	) );


	//Includes
	$includes = array(
		'Bootstrap Nav Walker' => 'inc/wp_bootstrap_navwalker.php',
		'ACF' => 'inc/include_acf.php',
		'Featured Products' => 'inc/featured_products.php',
		'Stores' => 'inc/stores.php',
		'MISC' => 'inc/misc.php',
		'Product Nav' => 'inc/product_nav.php',
		'Social Header' => 'inc/social_header.php',
		'Flexible' => 'inc/flexible.php'
	);
	foreach($includes as $key => $value) {
		// Locate Include
		$located = locate_template($value);
		if ( !empty($located)) {
			require_once($located);
		} else {
			echo '<strong>' . $key . ' missing<br></strong>';
		}
	}



	//Enqueue
	function theme_name_scripts() {
		wp_enqueue_style( 'layout', get_template_directory_uri() . '/style.css', array(), false );
		wp_enqueue_script('jquery');
		wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0', true );
		wp_enqueue_script('lazy-sizes', get_template_directory_uri() . '/js/lazysizes.min.js', array(), '1.0', true );
		wp_enqueue_script('instafeed', get_template_directory_uri() . '/js/instafeed.min.js', array(), '1.0', true );
		wp_enqueue_script('validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array(), '1.0', true );

		// register styles
	      wp_register_style('googlefont-duckhead', '//fonts.googleapis.com/css?family=Maven+Pro:500,500italic,700,700italic|Maven+Pro:400|Lato:700|Open+Sans+Condensed:300&amp;subset=latin,latin-ext', array(), false, 'all');

	        // enqueue styles
	      wp_enqueue_style('googlefont-duckhead');
	}

	add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
	
	if( function_exists('acf_add_options_sub_page') )
	{
	    acf_add_options_sub_page(array(
	        'title' => 'Site Options',
	        'parent' => 'options-general.php',
	        'capability' => 'manage_options'
	    ));
	}



	





add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );


function show_product_colors() {
		
	if( have_rows('product_color_repeater') ):
	 echo '<div class="product-colors-listing">';
	 	// loop through the rows of data
	    while ( have_rows('product_color_repeater') ) : the_row();
	 
	        // display a sub field value
	        ?>
	        <?php 
	        	$color = get_sub_field('product_color');
	        	$image = get_sub_field('product_color_image');
	        ?>
	        	<a href="<?php the_permalink(); ?>">
	        	<?php if($color != '') { ?>
	        		<div class="archive-swatch" style="background-color:<?php the_sub_field('product_color'); ?>"></div></a>
	        	<?php } elseif($image != '') { ?>
	        		<div  class="archive-swatch" style="background:url('<?php echo $image['url']; ?>')"></div></a>
	        	 <?php } ?>

	        <?php
	 
	    endwhile;
	 echo '</div>';
	else :
	 
	    // no rows found
	 
	endif;
	
}


function show_article_url() {
	// check if the repeater field has rows of data
	if( have_rows('link_to_article') ):

	 	// loop through the rows of data
	    while ( have_rows('link_to_article') ) : the_row();

	        // display a sub field value
	        $link = get_sub_field('link');
	        $link_title = get_sub_field('link_title');
	        if($link != ''):
		        $url = validate_url($link);
		        echo '<a href="' . $url . '">' . $link_title . '</a>';
		    endif;

	    endwhile;

	else :

	    // no rows found

	endif;
}

function show_slideshow() {

	// check if the repeater field has rows of data
	if( have_rows('slideshow') ):

		$count = 0;
	    // loop through the rows of data
	    while ( have_rows('slideshow') ) : the_row();
	    	$count++;
	        // display a sub field value
	        $image = get_sub_field('image');
	        $link = get_sub_field('link');
	        if($count == 1) :
		        echo '
			    	<div class="item active">
			    		' . show_image($image, $link, $mini_size='medium') . 
			    	'</div>';
			else : 
				echo '
			    	<div class="item">
			    		' . show_image($image, $link, $mini_size='medium') . 
			    	'</div>';
			endif;

	    endwhile;

	else :

	    // no rows found

	endif;

}