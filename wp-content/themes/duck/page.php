<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<main id="main">
    <div class="container">

        <?php 
            if(is_page('my-account') || is_page('checkout') || is_page('cart') || is_page('contact-us')):
                echo '
                    <div class="row"><div class="col-md-12">
                    <h1 class="page-title">';
                    the_title(); 
                echo '</h1><div class="shop-header"></div></div></div>';
                echo '<div class="row">';
                    echo '<div class="col-md-10 col-md-push-1 ">';
                        the_content();
                    echo '</div>';
                echo '</div>'; // Row
            elseif(is_page('shop')):

            else:
                echo '<div class="col-md-12">
                    <h1 class="page-title">' 
                        . get_the_title() . 
                    '</h1><div class="shop-header"></div></div>';
                $id = get_queried_object_id();
                show_flexible($id);
            endif;
        ?>
    </div>
</main>
<?php endwhile; else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
<?php get_footer(); ?>