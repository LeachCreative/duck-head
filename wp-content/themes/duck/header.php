<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="author" content="">

 	<title>Project Title</title>

   <?php wp_head(); ?>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php /*
    <link rel="shortcut icon" href="gerbing-favicon.png" />
	  <link rel="icon" type="image/vnd.microsoft.icon" href="gerbing-favicon.ico" />
    */ ?>
    
  </head>

  <body <?php body_class(); ?>>
  <div class="page-top">
    <div class="container">
      <div class="row">
        <div class="col-md-7"><p>Made In USA &nbsp;&nbsp;&nbsp; <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Contact Us' ) ) ); ?>">Contact Us</a> | <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'My Account' ) ) ); ?>">My Account</a> &nbsp;&nbsp;&nbsp;Free Shipping on Orders Over $150</p></div>
        <div class="col-md-2 pull-right">
          <?php show_social_header(); ?>
        </div>
      </div>
    </div>
  </div>
  <header>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <?php
            $img = get_field('logo', 'options');
            if(!empty($img)):
              echo '<a href="' . home_url() . '"><img src="' . $img['url'] . '"></a>';
            endif;
          ?>
        </div>
      </div>
    </div>
  </header>
  <nav class="main-nav">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container_class' => 'my_extra_menu_class' ) ); ?>
        </div>
      </div>
    </div>
  </nav>
  <nav class="product-nav">
    <div class="container">
      <div class="row">
        <div class="col-md-12 nav-wrap">
          <?php show_product_nav(); ?>
          <div class="col-md-12 nav-border-top"></div>
        </div>
      </div>
    </div>
  </nav>
