<?php
/*
Template Name: Search Page
*/
?>
<?php get_header(); ?>
<main id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-title">
                    Search Results
                </h1><div class="shop-header"></div>
            </div>
            <?php
                $myquery = "&posts_per_page=5"; // set to number of results you want per page
                $myquery = $query_string.$myquery;
                query_posts($myquery);
            ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="col-md-4">
                    <?php the_post_thumbnail(); ?>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                </div>
            <?php endwhile; else: ?>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>
        </div>
    </div>
</main>

<?php get_footer(); ?>