<?php
  function show_product_nav() {
      // check if the repeater field has rows of data
      if( have_rows('product_categories_nav', 'options') ):
        $row_count = count(get_field('product_categories_nav', 'options'));
        $count = 0;
        // loop through the rows of data
        while ( have_rows('product_categories_nav', 'options') ) : the_row();

            $count ++;

            // display a sub field value
            $image = get_sub_field('image', 'options');
            $link = get_sub_field('link', 'options');
            echo '<div class="col-md-2 ';
              if($count == $row_count) :
                echo 'last';
              endif;

            echo '">';
              if($link != '') {
                echo show_image($image, $link, 'medium');
              } else {
                echo show_image($image, '', 'medium');
              }

              get_sub_nav();

            echo '</div>';

        endwhile;

      else :

        // no rows found

      endif;
  }


  function get_sub_nav() {
  // check if the repeater field has rows of data
    if( have_rows('sub_nav', 'options') ):
      echo '<ul class="sub-nav">';
      // loop through the rows of data
      while ( have_rows('sub_nav', 'options') ) : the_row();

        // display a sub field value
        $name = get_sub_field('sub_nav_item_name', 'options');
        $link = get_sub_field('sub_nav_item_link', 'options');
        echo '<li><a href="'. $link .'">' . $name . '</a>';

        endwhile;
        echo '</ul>';
    else :

      // no rows found

    endif;
}
?>