<div class="row"><!-- Row 1 -->
    <div class="col-md-8">
        <?php 
            $img = get_field('row_1_image_left');
            if(!empty($img)):
                $size = 'medium';
                $medium = $image['sizes'][ $size ];
                echo '<img alt="' . $img['alt'] . '"  src="' . $medium . '" data-src="' . $img['url'] . '" class="lazyload" />';
            endif;
        ?>
    </div>
    <div class="col-md-4">
        <?php 
            $img = get_field('row_1_image_right');
            if(!empty($img)):
                $size = 'medium';
                $medium = $image['sizes'][ $size ];
                echo '<img alt="' . $img['alt'] . '"  src="' . $medium . '" data-src="' . $img['url'] . '" class="lazyload" />';
            endif;
        ?>
    </div>
</div><!-- Row 2 -->
    

<?php 
    $id = get_queried_object_id();
    show_featured($id); 
?>


<!-- Row 3 -->
<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-4">
                <?php 
                    $img = get_field('row_2_image_left_top');
                    if(!empty($img)):
                        $size = 'medium';
                        $medium = $image['sizes'][ $size ];
                        echo '<img alt="' . $img['alt'] . '"  src="' . $medium . '" data-src="' . $img['url'] . '" class="lazyload" />';
                    endif;
                ?>
            </div>
            <div class="col-md-8">
                <?php 
                    $img = get_field('row_2_image_right_top');
                    if(!empty($img)):
                        $size = 'medium';
                        $medium = $image['sizes'][ $size ];
                        echo '<img alt="' . $img['alt'] . '"  src="' . $medium . '" data-src="' . $img['url'] . '" class="lazyload" />';
                    endif;
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <?php 
                    $img = get_field('row_2_image_left_bottom');
                    if(!empty($img)):
                        $size = 'medium';
                        $medium = $image['sizes'][ $size ];
                        echo '<img alt="' . $img['alt'] . '"  src="' . $medium . '" data-src="' . $img['url'] . '" class="lazyload" />';
                    endif;
                ?>
            </div>
            <div class="col-md-4">
                <?php 
                    $img = get_field('row_2_image_right_bottom');
                    if(!empty($img)):
                        $size = 'medium';
                        $medium = $image['sizes'][ $size ];
                        echo '<img alt="' . $img['alt'] . '"  src="' . $medium . '" data-src="' . $img['url'] . '" class="lazyload" />';
                    endif;
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <?php 
            $img = get_field('row_2_image_right_full');
            if(!empty($img)):
                $size = 'medium';
                $medium = $image['sizes'][ $size ];
                echo '<img alt="' . $img['alt'] . '"  src="' . $medium . '" data-src="' . $img['url'] . '" class="lazyload" />';
            endif;
        ?>
    </div>
</div><!-- Row 3 -->
<div class="row">
    <div class="col-md-8">
        <?php 
            $img = get_field('row_4_image_left');
            if(!empty($img)):
                $size = 'medium';
                $medium = $image['sizes'][ $size ];
                echo '<img alt="' . $img['alt'] . '"  src="' . $medium . '" data-src="' . $img['url'] . '" class="lazyload" />';
            endif;
        ?>
    </div>
    <div class="col-md-4">
        <?php 
            $img = get_field('row_4_image_right');
            if(!empty($img)):
                $size = 'medium';
                $medium = $image['sizes'][ $size ];
                echo '<img alt="' . $img['alt'] . '"  src="' . $medium . '" data-src="' . $img['url'] . '" class="lazyload" />';
            endif;
        ?>
    </div>
</div>