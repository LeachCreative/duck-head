<?php

    function validate_url($urlStr) {
        $parsed = parse_url($urlStr);
        if (empty($parsed['scheme'])) {
            $urlStr = 'http://' . ltrim($urlStr, '/');
        }
        return $urlStr;
    }

        
    /**************************************
        Show Image
        Checks to see if image exists
        then pulls several versions and 
        plugs it into lazy load
    **************************************/
    function show_image($img, $link = '', $mini_size='medium') {
        if(!empty($img)):
            if($link != ''):
                $size_image = $img['sizes'][ $mini_size ];
                $reg_image = $img['url'];
                $image = '<a href="' . $link . '"><img alt="' . $img['alt'] . '"  src="' . $size_image . '" data-src="' . $reg_image . '" class="lazyload" /></a>';
                return $image;
            else:
                $size_image = $img['sizes'][ $mini_size ];
                $reg_image = $img['url'];
                $image = '<img alt="' . $img['alt'] . '"  src="' . $size_image . '" data-src="' . $reg_image . '" class="lazyload" />';
                return $image;
            endif;
        else:
            return 'No Image';
        endif;
    }



?>