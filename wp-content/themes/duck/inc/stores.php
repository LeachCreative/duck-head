<?php

        /* Do something with the data entered */
    add_action( 'save_post', 'set_store_name' );

    /* When the post is saved, saves our custom data */
    function set_store_name( $post_id ) {

        // First we need to check if the current user is authorised to do this action. 
        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) )
                return;
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) )
                return;
        }

        $title = $_POST['post_title']; // Do something with $mydata 

        update_post_meta( $post_id, 'store-name', $title );

    }


function partition( $list, $p ) {
    $listlen = count( $list );
    $partlen = floor( $listlen / $p );
    $partrem = $listlen % $p;
    $partition = array();
    $mark = 0;
    for ($px = 0; $px < $p; $px++) {
        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
        $partition[$px] = array_slice( $list, $mark, $incr );
        $mark += $incr;
    }
    return $partition;
}

function show_store_column($store_column, $column = 0, $show_state = true) {
    foreach ($store_column[$column] as &$store) {
        if($show_state == true) {
            if($_SESSION['currState'] != $store['state']) :
                echo '<h3>' . $store['state'] . '</h3>';
                $_SESSION['currState'] = $store['state'];
            endif;
        }
        echo '<p>';
        echo '<strong>' . $store['store-name'] . '</strong><br>';
        //echo $store['state'] . '<br>';
        if ($store['address1'] != '') :
            echo $store['address1'] . '<br>';
        endif;
        if ($store['address2'] != '') :
            echo $store['address2'] . '<br>';
        endif;
        if ($store['hours'] != '') :
            echo $store['hours'] . '<br>';
        endif;
        if ($store['phone'] != '') :
            echo $store['phone'] . '<br>';
        endif;
        if ($store['website'] != '') :
            echo '<a href="' . validate_url($store['website']) . '">' . $store['website'] . '</a>';
        endif;
        echo '</p>';
    }
}


function get_stores($num_columns = 3) { 
    $args = array(
        'post_type'=>'stores',
        'orderby' => 'meta_value',
        'posts_per_page' => -1,
        'meta_key' => 'state' + 'store-name' + 'domestic',
        'order' => 'ASC',
        'meta_query' => array(
            array(
                'key' => 'state',
            ),
            array(
                'key' => 'store-name',
            ),
            array (
                'key' => 'domestic',
            )
        )
    );
    query_posts($args); 

    $count == 0;
    $dh_count = 0;
    if ( have_posts() ) : while ( have_posts() ) : the_post(); 
        if(get_field('domestic') == 'Yes') {
            if(get_field('duck_head_owned') == 'No') {
                $stores[$count]['store-name'] = get_field('store-name');
                $stores[$count]['state'] = get_field('state');
                $stores[$count]['address1'] = get_field('address_line_1');
                $stores[$count]['address2'] = get_field('address_line_2');
                $stores[$count]['hours'] = get_field('hours');
                $stores[$count]['phone'] = get_field('phone');
                $stores[$count]['website'] = get_field('link');
                $count++;
            } elseif(get_field('duck_head_owned') == 'Yes') {
                $dh_stores[$dh_count]['store-name'] = get_field('store-name');
                $dh_stores[$dh_count]['state'] = get_field('state');
                $dh_stores[$dh_count]['address1'] = get_field('address_line_1');
                $dh_stores[$dh_count]['address2'] = get_field('address_line_2');
                $dh_stores[$dh_count]['hours'] = get_field('hours');
                $dh_stores[$dh_count]['phone'] = get_field('phone');
                $dh_stores[$dh_count]['website'] = get_field('link');
                $dh_count++;
            }
        }  
    endwhile; else : 
    
    endif; 


    $args = array(
        'post_type'=>'stores',
        'orderby' => 'meta_value',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'meta_key' => 'domestic',
        'meta_value' => 'No'
    );
    query_posts($args); 

    $intl_count = 0;
    
    if ( have_posts() ) : while ( have_posts() ) : the_post(); 
        if(get_field('domestic') == 'No') {
            $intl_stores[$intl_count]['store-name'] = get_field('store-name');
            //$stores[$count]['state'] = get_field('state');
            $intl_stores[$intl_count]['address1'] = get_field('address_line_1');
            $intl_stores[$intl_count]['address2'] = get_field('address_line_2');
            $intl_stores[$intl_count]['hours'] = get_field('hours');
            $intl_stores[$intl_count]['phone'] = get_field('phone');
            $intl_stores[$intl_count]['website'] = get_field('link');
            $intl_count++;
        } 
    endwhile;endif;

    $store_column = partition($stores, $num_columns);
    $dh_column = partition($dh_stores, $num_columns);
    $intl_column = partition($intl_stores, $num_columns);

    $stores = array($store_column, $dh_column, $intl_column);

    return $stores;    

}

function show_stores($stores, $num_columns, $show_state = true) {
    for($count = 0; $count < $num_columns; $count++) { 
        echo '<div class="col-md-' . ($num_columns + 1) . '">';
            show_store_column($stores, $count, $show_state);
        echo '</div>';
    }
}

?>