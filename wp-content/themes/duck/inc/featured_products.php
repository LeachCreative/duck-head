<?php

    function show_featured($post_id) {
        
            // check if the repeater field has rows of data
            if( have_rows('featured_products', $post_id) ):
                $count = 0 ;
                echo '<div class="row">';
                // loop through the rows of data
                while ( have_rows('featured_products', $post_id) ) : the_row();

                    if($count != 0) :
                        // display a sub field value
                        echo '<div class="col-md-3 margin-top-20">';
                            $link = get_sub_field('link', $post_id);
                            $img = get_sub_field('image', $post_id);
                            echo show_image($img, $link, 'medium');
                            
                        echo '</div>';
                    else :
                        // display a sub field value
                        echo '<div class="col-md-3">';

                            $img = get_sub_field('image', $post_id);
                            $link = get_sub_field('link', $post_id);
                            echo show_image($img, $link, 'medium');
                            
                        echo '</div>';
                    endif;
                    $count++;
                endwhile;
                echo '</div>';
            else :
                // no rows found
            endif;
    }

?>