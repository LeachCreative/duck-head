<?php
    /*********************************
        Flexible Content Field
    *********************************/
    function show_flexible($post_id) {
        // check if the flexible content field has rows of data
        if( have_rows('flexible_content', $post_id) ):

             // loop through the rows of data
            while ( have_rows('flexible_content', $post_id) ) : the_row();


                // 3 Column Image Block
                if( get_row_layout() == 'three_image_block' ):

                    show_three_image_block($post_id);                   

                elseif( get_row_layout() == 'full_width_image' ): 

                    show_full_width_image($post_id);

                elseif( get_row_layout() == 'large_image_left_small_right' ): 

                    show_large_image_left_small_right($post_id);

                elseif( get_row_layout() == 'content_with_double_border' ): 

                    show_content_with_double_border($post_id);

                elseif( get_row_layout() == 'small_image_left_large_right' ): 

                    show_small_image_left_large_right($post_id);

                elseif( get_row_layout() == 'content_1_column_offset' ): 

                    show_content_1_column_offset($post_id);

                elseif( get_row_layout() == 'accordion' ): 

                    show_accordion($post_id);

                endif;

            endwhile;

        else :

            // no layouts found

        endif;
    }

    /***************************************
        Flexible Content 
        3 Image Block
    ***************************************/
    function show_three_image_block($post_id) {
        $image_left = get_sub_field('image_left', $post_id);
        $image_middle = get_sub_field('image_middle', $post_id);
        $image_right = get_sub_field('image_right', $post_id);

        echo    
        '<div class="row">
            <div class="col-md-3">'
                . show_image($image_left) .
            '</div>
            <div class="col-md-6">'
                . show_image($image_middle) .
            '</div>
            <div class="col-md-3">'
                . show_image($image_right) .
            '</div>
        </div><!-- Row -->';
    }

    /***************************************
        Flexible Content 
        Large Image Left Small Right
    ***************************************/
    function show_large_image_left_small_right($post_id) {

        $image_left = get_sub_field('image_left', $post_id);
        $image_right = get_sub_field('image_right', $post_id);

        echo 
        '<div class="row">
            <div class="col-md-8">'
                . show_image($image_left) .
            '</div>
            <div class="col-md-4">'
                . show_image($image_right) .
            '</div>
        </div>';
    }

    /***************************************
        Flexible Content 
        Full Width Image
    ***************************************/
    function show_full_width_image($post_id) {

        $image = get_sub_field('image', $post_id);       
        echo 
        '<div class="row">
            <div class="col-md-12">';
                echo show_image($image, 'medium', 'full');
            echo '</div>
        </div>';
    }
    /**************************************
        Flexible Content
        Double Border Content
    **************************************/
    function show_content_with_double_border($post_id) {
        $content = get_sub_field('content');

        echo '
            <div class="row">
                <div class="col-md-12">
                    <div class="double-border">
                    ' . $content . '
                    </div>
                </div>
            </div>'; // Row
    }
    /**************************************
        Flexible Content
        Small Image Left
        Large Right
    **************************************/
    function show_small_image_left_large_right($post_id) {
        $image_left = get_sub_field('image_left', $post_id);
        $image_right = get_sub_field('image_right', $post_id);

        echo 

        '<div class="row">
            <div class="col-md-4>' .
                show_image($image_left) .
            '</div>
            <div class="col-md-8>' .
                show_image($image_right) .
            '</div>
        </div>';
    }
    /**************************************
        Flexible Content
        Content 1 Column Offset
    **************************************/
    function show_content_1_column_offset($post_id) {
        $content = get_sub_field('content', $post_id);

        echo 
            '<div class="row">
                <div class="col-md-10 col-md-push-1">
                    '. $content . '
                </div>
            </div>'; // Row
            
    }
    function show_accordion($post_id) {
        echo '<div class="col-md-10 col-md-push-1">';
        echo '<h2>' . get_sub_field('accordion_title', $post_id) . '</h2>';
        echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
        // check if the repeater field has rows of data
        if( have_rows('accordion_group') ):
            
            // loop through the rows of data
            while ( have_rows('accordion_group') ) : the_row();

                // display a sub field value
                $title = get_sub_field('title');
                $content = get_sub_field('content');


                echo '
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading' . $_SESSION['count'] . '">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse' . $_SESSION['count'] . '" aria-expanded="true" aria-controls="collapse' . $_SESSION['count'] . '">
                          ' . $title . '
                        </a>
                      </h4>
                    </div>
                    <div id="collapse' . $_SESSION['count'] . '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' . $_SESSION['count'] . '">
                      <div class="panel-body">
                      ' . $content . '
                      </div>
                    </div>
                  </div>';
                  $_SESSION['count']++;
            endwhile;

        else :

            // no rows found

        endif;
        echo '</div></div>';
    }
?>