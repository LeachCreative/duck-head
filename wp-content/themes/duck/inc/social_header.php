<?php
  function show_social_header() {
    // check if the repeater field has rows of data
    if( have_rows('social_icons', 'options') ):
        echo '<ul class="header-social-icons">';
          // loop through the rows of data
          while ( have_rows('social_icons', 'options') ) : the_row();

              echo '<li>';
              // display a sub field value
              $image = get_sub_field('social_icon', 'options');
              $link = get_sub_field('social_icon_link', 'options');
              
              if($link != '') {
                echo show_image($image, $link, 'medium');
              } else {
                echo show_image($image, '', 'medium');
              }
              echo '</li>';
              

          endwhile;
        echo '</ul>';
    else :

        // no rows found

    endif;
  }

  function show_social_footer() {
    // check if the repeater field has rows of data
    if( have_rows('social_icons', 'options') ):
        echo '<ul class="footer-social-icons">';
          // loop through the rows of data
          while ( have_rows('social_icons', 'options') ) : the_row();

              echo '<li>';
              // display a sub field value
              $image = get_sub_field('social_icon', 'options');
              $link = get_sub_field('social_icon_link', 'options');
              
              if($link != '') {
                echo show_image($image, $link, 'medium');
              } else {
                echo show_image($image, '', 'medium');
              }
              echo '</li>';
              

          endwhile;
        echo '</ul>';
    else :

        // no rows found

    endif;
  }
?>