=== WooCommerce CyberSource Gateway ===
Author: skyverge
Tags: woocommerce
Requires WooCommerce at least: 2.1

== Installation ==

 * Upload the plugin files to the '/wp-content/plugins/' directory
 * Activate the plugin through the 'Plugins' menu in WordPress
 * Go to your WooCommerce payment Gateway settings page and enable/configure the gateway

== Configuration ==

 * Log into your CyberSource Business Center
 * Find or generate a transaction security key by going to Account Management > Transaction Security Keys > Security Keys for the SOAP Toolkit API and clicking 'Generate'.  Configure this key in the plugin Settings
 * Merchant ID: this is the id you use to log into your CyberSource Business Center
 * Make sure to disable test/debug modes in the WooCommerce Settings page prior to accepting live transactions!
